package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/akamensky/argparse"
)

func online(retries int, wait time.Duration) (*SshClient, error) {
	fmt.Fprintln(os.Stderr, "waiting for the ssh connection to become ready")

	for i := 0; i < retries; i++ {
		client, err := NewClient()
		if err == nil {
			return client, nil
		}
		time.Sleep(time.Millisecond * wait)
	}

	errMsg := fmt.Sprintf("SSH Connection failed after %d retries.\n", retries)
	return nil, errors.New(errMsg)
}

func flash(c *SshClient, filename string) error {
	fmt.Fprintln(os.Stderr, "starting!")
	defer fmt.Fprintln(os.Stderr, "firmware flashed successfully")

	fmt.Fprintln(os.Stderr, "determine kernel and boot-selector dev")
	kernel0, err := c.Command("cat /proc/mtd | awk -F  ':' '/\"kernel0\"$/{ print substr($1,4)}'")
	bootSelector, err := c.Command("cat /proc/mtd | awk -F  ':' '/\"bs\"$/{ print substr($1,4)}'")
	if err != nil {
		return err
	}

	if err := c.Scp(filename); err != nil {
		return err
	}
	fmt.Fprintln(os.Stderr, "removing write protection")
	if _, err := c.Command("if [ -f /proc/ubnthal/.uf ]; then echo '5edfacbf' > /proc/ubnthal/.uf; fi"); err != nil {
		return err
	}

	fmt.Fprintf(os.Stderr, "writing firmware to /dev/mtdblock%q and setting /dev/mtd%q to boot from it", kernel0, bootSelector)
	if _, err := c.Command("dd if=/tmp/firmware.bin of=/dev/mtdblock" + kernel0); err != nil {
		return err
	}

	if _, err := c.Command("dd if=/dev/zero bs=1 count=1 of=/dev/mtd" + bootSelector); err != nil {
		return err
	}

	if _, err := c.Command("reboot"); err != nil {
		return err
	}

	fmt.Fprintln(os.Stderr, "rebooting")
	return nil
}

func main() {
	description := "A tool to automatically load custom firmware and flash it on an ubiquiti UAP-AC-PRO with missing " +
		"mtd tooling and write-protection. (firmwareversion ≥  4.3.x)"

	parser := argparse.NewParser("firmwareloader", description)

	endlessMode := parser.Flag("e", "endless",
		&argparse.Options{
			Required: false,
			Help:     "Putting tool in an endless loop mode",
		})

	filename := parser.String("f", "file",
		&argparse.Options{
			Required: false,
			Default:  "firmware.bin",
			Help:     "Path to firmware",
		})

	if err := parser.Parse(os.Args); err != nil {
		panic(err)
	}

	for {
		client, err := online(20, 500)
		if err == nil {
			flashErr := flash(client, *filename)
			if flashErr != nil {
				fmt.Fprintln(os.Stderr, flashErr)
			}
			if !*endlessMode {
				break
			}
		} else {
			_, _ = fmt.Fprintln(os.Stderr, "AP not ready!")
		}
	}
}
