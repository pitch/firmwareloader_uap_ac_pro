BINARY_NAME=firmwareloader
BUILD_DIR=dist

build:
	mkdir -p ${BUILD_DIR}
	GOOS=linux GOARCH=amd64 go build -o ${BUILD_DIR}/${BINARY_NAME}-linux
	GOOS=linux GOARCH=386 go build -o ${BUILD_DIR}/${BINARY_NAME}-linux-i386
	GOOS=darwin GOARCH=amd64 go build -o ${BUILD_DIR}/${BINARY_NAME}-macOS-intel
	GOOS=darwin GOARCH=arm64 go build -o ${BUILD_DIR}/${BINARY_NAME}-macOS-arm
	GOOS=windows GOARCH=amd64 go build -o ${BUILD_DIR}/${BINARY_NAME}-win.exe
	GOOS=windows GOARCH=386 go build -o ${BUILD_DIR}/${BINARY_NAME}-win32.exe


