module firmwareloader_uap_ac_pro

go 1.19

require (
	github.com/akamensky/argparse v1.4.0
	golang.org/x/crypto v0.0.0-20220926161630-eccd6366d1be
)

require golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
