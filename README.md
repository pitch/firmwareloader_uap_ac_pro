# acProFlashtool
setup your machine to have a route and address, so it can reach the ACpro via 192.168.1.20. check with 
`ssh -o HostKeyAlgorithms=ssh-rsa ubnt@192.168.1.20` if it prompts for a password you are good to go.  
then run this utility in a directory containing firmware.bin, this file will be transferred and flashed
onto the kernel0 blockdevice of the connected ACpro.  
See `-h`|`--help` for further functionality.

## THANKS
blocktrron - for providing insight on how to remove the write-protection from kernel blocks -
[source (german)](https://forum.freifunk.net/t/scp-tftp-downgrade-bei-unifi-ac-ab-firmware-4-3-x-geht-nicht-mehr/23422/10) 


## TODO
* investigate how applicable the software is to other ubiquiti products
* better error handling
* detection/selection of .bin file in the pwd
* take filename as an argument without flag

## License

Code is under the [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](LICENSE).  
