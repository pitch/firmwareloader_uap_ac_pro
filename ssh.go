package main

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"os"
	"time"
)

type SshClient struct {
	client *ssh.Client
}

func NewClient() (*SshClient, error) {
	config := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         time.Millisecond * 100,
		User:            "ubnt",
		Auth: []ssh.AuthMethod{
			ssh.Password("ubnt"),
		},
	}

	client, err := ssh.Dial("tcp", "192.168.1.20:22", config)
	if err != nil {
		return nil, err
	}

	return &SshClient{client: client}, err
}

func (c *SshClient) Scp(filename string) error {
	fmt.Fprint(os.Stderr, "scp the firmware ")
	defer fmt.Fprintln(os.Stderr, " done!")

	session, err := c.client.NewSession()
	if err != nil {
		return err
	}
	defer func() {
		_ = session.Close()
	}()

	file, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	go func() {
		fmt.Fprint(os.Stderr, ".")
		defer fmt.Fprint(os.Stderr, ".")

		pipe, _ := session.StdinPipe()
		defer pipe.Close()

		fmt.Fprintln(pipe, "C0644", len(file), "firmware.bin")
		pipe.Write(file)
		fmt.Fprint(pipe, "\x00")
	}()

	if err := session.Run("/usr/bin/env scp -t /tmp/"); err != nil {
		return err
	}

	fmt.Fprint(os.Stderr, ".")
	return nil
}

func (c *SshClient) Command(cmd string) (string, error) {
	fmt.Fprint(os.Stderr, "invoking "+cmd+" on AP.")
	defer fmt.Fprintln(os.Stderr, "done!")

	var stdOut bytes.Buffer
	session, err := c.client.NewSession()

	if err != nil {
		return "", err
	}
	defer session.Close()
	fmt.Fprint(os.Stderr, ".")

	session.Stdout = &stdOut
	err = session.Run(cmd)
	return stdOut.String(), err
}
